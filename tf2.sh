#!/usr/bin/env bash

# v2.sh -h <optional custom home> -s <steamcmd path (fallsback to $PATH if not supplied)> -m <map>

set -e

echoerr() {
    echo "Error: $@" 1>&2
    exit 1
}

# Set default variables
which steamcmd 2>/dev/null && STEAMCMD_DIR="$(dirname $(which steamcmd))" && STEAMCMD_BIN="steamcmd"
TF2_DEFAULT_MAP="itemtest"

while getopts "h:s:m:" opt; do
  case $opt in
    h)
      HOME="$OPTARG"
      ;;
    s)
      STEAMCMD_DIR="$(dirname $OPTARG)"
      STEAMCMD_BIN="$(basename $OPTARG)"
      ;;
    m)
      TF2_DEFAULT_MAP="$OPTARG"
      ;;
  esac
done

if [ ! "$STEAMCMD_BIN" ] || [ ! "$STEAMCMD_DIR" ] ; then
    echoerr "steamcmd not found. Add it to your path or supply its path with -s"
fi

_steamcmd() {
    # The parantheses are used to start in a subshell
    (cd "$STEAMCMD_DIR"; exec ./"$STEAMCMD_BIN" "$@")
}

# Update
_steamcmd "+login anonymous" "+app_update 232250" "+quit"

# Set $TF2_PATH to reuse
TF2_PATH="$HOME"/Steam/steamapps/common/"Team Fortress 2 Dedicated Server"

# Update config from $URL
URL="https://gitlab.com/ArtixBTW/tf2-cfg/-/raw/main/tf/custom/artix/cfg/overrides/listenserver.cfg"
mkdir -p "${TF2_PATH}"/tf/cfg
curl -s "$URL" > "${TF2_PATH}"/tf/cfg/server.cfg

# Run server
"${TF2_PATH}"/srcds_run "+exec server" "+map ${TF2_DEFAULT_MAP}"
